import React from 'react';
import './header.styles.css';

export const Header = () => (
    <div className="header">
        <h2 className="title">To-Do-App</h2>
    </div>
);